<?php declare(strict_types=1);

namespace davidschmucker\jsonschemamerger\JsonSchemaMerger;

use davidschmucker\jsonschemamerger\JsonSchema\JsonSchema;

interface JsonSchemaMerger
{
  public function getMergedSchema(): JsonSchema;
}