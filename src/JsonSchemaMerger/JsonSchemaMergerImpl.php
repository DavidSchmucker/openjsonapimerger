<?php declare(strict_types=1);

namespace davidschmucker\jsonschemamerger\JsonSchemaMerger;

use davidschmucker\jsonschemamerger\JsonSchema\JsonSchema;
use davidschmucker\jsonschemamerger\JsonSchema\JsonSchemaFactory;
use davidschmucker\jsonschemamerger\JsonSchema\JsonSchemaImpl;

class JsonSchemaMergerImpl implements JsonSchemaMerger
{
  private const REGEX_ALTERNATIVE_SIGN = '|';

  private const ALLOWED_META_KEYWORDS = ['$defs'];

  private array $supportedUriSchema = ['http', 'https', 'ns'];

  private JsonSchemaFactory $schemaFactory;

  private JsonSchema $startSchema;

  private JsonSchema $mergedSchema;

  public function __construct(JsonSchema $startSchema, JsonSchemaFactory $schemaFactory)
  {
    $this->startSchema = $startSchema;
    $this->mergedSchema = $startSchema;
    $this->schemaFactory = $schemaFactory;

    $this->mergeSchemas();
  }

  private function mergeSchemas(): void
  {
    $mergedSchemaArray = $this->startSchema->getSchemaArray();
    $continueSchemaRefCheck = true;
    while($continueSchemaRefCheck)
    {
      $mergedSchemaArray = $this->replaceReferencesInSchemaArray($mergedSchemaArray, JsonSchema::REFERENCE_KEY);
      $compareSchemaArray = $this->replaceReferencesInSchemaArray($mergedSchemaArray, JsonSchema::REFERENCE_KEY);
      $continueSchemaRefCheck = ($mergedSchemaArray !== $compareSchemaArray);
    }

    $schemaFactory = new JsonSchemaFactory();
    $this->mergedSchema = $schemaFactory->createFromArray($mergedSchemaArray);

    // TODO - remove defenitions
  }

  private function replaceReferencesInSchemaArray(array $array, string $find): array
  {
    foreach($array as $key => $value) 
    {
      if(is_array($array[$key]))
        $array[$key] = $this->replaceReferencesInSchemaArray($array[$key], $find);
      else
        if($key === $find)
          $array = $this->getReferenceReplacement($array[$key]);
    }

    return $array;
  }
  
  private function getReferenceReplacement(string $reference): array
  {
    $replacement = $this->getReplacementByReferenceType($reference);

    return $replacement;
  }

  private function getReplacementByReferenceType(string $reference): array
  {
    $subSchemaRegex = sprintf('"^%s/\$defs(?:%s\w{1,}){1,}$"', JsonSchema::SUBSCHEMA_CHAR, JsonSchema::POINTER_SEPERATOR);
    $relSchemaRegex = sprintf('"^(?:%s\w{1,}){1,}$"', JsonSchema::POINTER_SEPERATOR);
    $uriSchemeRegex = sprintf('"^(%s)://"', implode(self::REGEX_ALTERNATIVE_SIGN, $this->supportedUriSchema));
    $fileDefaultRegex = '"^[a-zA-Z0-9_](?:(?!://).)*$"';

    $replacement = [];

    if(preg_match($subSchemaRegex, $reference))
      $replacement = $this->getReplacementBySubschema($reference);
    elseif(preg_match($relSchemaRegex, $reference))
      $replacement = $this->getReplacementByRelative($reference);
    elseif(preg_match($uriSchemeRegex, $reference))
    {
      // TODO - uri schema handling
    }
    elseif(preg_match($fileDefaultRegex, $reference))
    {
      // TODO - defautl start with file
    }
    else
    {
      // TODO - what else
    }

    return $replacement;
  }

  private function getReplacementBySubschema(string $reference): array
  {
    $referenceSplit = array_slice(explode(JsonSchema::POINTER_SEPERATOR, $reference), 1);
    $schemaArray = $this->mergedSchema->getSchemaArray();

    $replacementArray = $this->getReplacementArrayByIndexArray($schemaArray, $referenceSplit);
    
    $replacementArray = $this->cleanJsonSchemaArrayFromMetaKeyword($replacementArray);

    return $replacementArray;
  } 

  private function getReplacementArrayByIndexArray(array $replacementArray, array $indexArray): array
  {
    foreach($indexArray as $schemaIndex)
      $replacementArray = $replacementArray[$schemaIndex]; 

    return $replacementArray;
  } 

  private function getReplacementByRelative(string $reference): array
  {
    $schemaArray = $this->mergedSchema->getSchemaArray();

    $replacementArray = [];
    foreach($schemaArray['$defs'] as $definitions)
      if(array_key_exists('$id', $definitions) && ($definitions['$id'] === $reference))
        $replacementArray = $this->cleanJsonSchemaArrayFromMetaKeyword($definitions);

    return $replacementArray;
  }

  private function cleanJsonSchemaArrayFromMetaKeyword(array $array): array
  {
    foreach($array as $index => $entry)
      if(str_starts_with($index, JsonSchema::META_KEYWORD_CHAR) && !in_array($index, JsonSchemaMergerImpl::ALLOWED_META_KEYWORDS))
        unset($array[$index]);
    
    return $array;
  }

  public function getMergedSchema(): JsonSchema
  {
    return $this->mergedSchema;
  }
}