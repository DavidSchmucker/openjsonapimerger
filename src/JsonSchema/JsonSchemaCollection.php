<?php declare(strict_types=1);

namespace davidschmucker\jsonschemamerger\JsonSchema;

final class JsonSchemaCollection implements \ArrayAccess, \Countable, \IteratorAggregate
{
  private array $jsonSchemas;

  public function __construct(JsonSchema ...$jsonSchemas)
  {
    foreach($jsonSchemas as $schema)
      if(count(array_keys($jsonSchemas, $schema, true)) > 1)
        throw new \Exception('Double value exists in Parameter!');

    $this->jsonSchemas = $jsonSchemas;
  }

  public function offsetExists(mixed $offset): bool
  {
    return isset($this->jsonSchemas[$offset]);
  }

  public function offsetGet(mixed $offset): JsonSchema
  {
    return $this->jsonSchemas[$offset];
  }

  public function offsetSet(mixed $offset, mixed $value): void
  {
    if(!($value instanceof JsonSchema))
      throw new \InvalidArgumentException('Value is not a JsonSchema!');

    if(in_array($value, $this->jsonSchemas, true))
      throw new \Exception('Value always exists in Collection!');

    $this->jsonSchemas[$offset] = $value;
  }

  public function offsetUnset(mixed $offset): void
  {
    unset($this->jsonSchemas[$offset]);
  }

  public function count(): int
  {
    return count($this->jsonSchemas);
  }

  public function getIterator(): \ArrayIterator
  {
    return new \ArrayIterator($this->jsonSchemas);
  }
}