<?php declare(strict_types=1);

namespace davidschmucker\jsonschemamerger\JsonSchema;

class JsonSchemaImpl implements JsonSchema
{
  private ?string $schemaString = null;
  private ?array $schemaArray = null;
  private ?\stdClass $schemaObject = null;

  public function __construct(?string $schemaString = null)
  {
    if(!is_null($schemaString))
    {
      $this->validateSchemaString($schemaString);

      $this->schemaString = $schemaString;
      $this->schemaArray = json_decode($this->schemaString, true);
      $this->schemaObject = json_decode($this->schemaString, false);
    }
  }

  private function validateSchemaString(string $schemaString): void
  {
    if(!$this->validateJson($schemaString))
      throw new \Exception('Unknown failure during Schema validation!');
  }

  private function validateJson(string $schemaString): bool
  {
    if(json_decode($schemaString) === null)
      throw new \Exception('Schema String doesn\'t contain a valid JSON!\n' . json_last_error_msg());

    return true;
  }

  public function getSchemaString(): ?string
  {
    return $this->schemaString;
  }

  public function getSchemaArray(): ?array
  {
    return $this->schemaArray;
  }

  public function getSchemaObject(): ?\stdClass  
  {
    return $this->schemaObject;
  }

  public function setSchemaByString(string $schemaString): void
  {
    $this->validateSchemaString($schemaString);

    $this->schemaString = $schemaString;
    $this->schemaArray = json_decode($this->schemaString, true);
    $this->schemaObject = json_decode($this->schemaString, false);
  }

  public function setSchemaByArray(array $schemaArray): void
  {
    $this->schemaArray = $schemaArray;
    $this->schemaString = json_encode($schemaArray);
    $this->schemaObject = json_decode($this->schemaString, false);
  }

  public function setSchemaByObject(\stdClass $schemaObject): void  
  {
    $this->schemaObject = $schemaObject;
    $this->schemaString = json_encode($schemaObject);
    $this->schemaArray = json_decode($this->schemaString, true);
  }
}