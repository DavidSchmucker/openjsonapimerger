<?php declare(strict_types=1);

namespace davidschmucker\jsonschemamerger\JsonSchema;

use davidschmucker\jsonschemamerger\FileHandler\FileHandlerImpl;

class JsonSchemaFactory
{
  public function createFromFile(string $path): JsonSchema
  {
    $fileHandler = new FileHandlerImpl($path);
    return new JsonSchemaImpl($fileHandler->getContent());
  }

  public function createFromArray(array $array): JsonSchema
  {
    $jsonSchema = new JsonSchemaImpl();
    $jsonSchema->setSchemaByArray($array);
    return $jsonSchema; 
  }
}