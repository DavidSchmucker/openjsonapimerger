<?php declare(strict_types=1);

namespace davidschmucker\jsonschemamerger\JsonSchema;

interface JsonSchema
{
  public const META_KEYWORD_CHAR = '$';
  public const SUBSCHEMA_CHAR = '#';
  public const POINTER_SEPERATOR = '/';
  public const REFERENCE_KEY = '$ref';

  public function getSchemaString(): ?string;
  public function getSchemaArray(): ?array;
  public function getSchemaObject(): ?\stdClass;
  public function setSchemaByString(string $schemaString): void;
  public function setSchemaByArray(array $schemaArray): void;
  public function setSchemaByObject(\stdClass $schemaObject): void;
}