<?php declare(strict_types=1);

namespace davidschmucker\jsonschemamerger\FileHandler;

interface FileHandler
{
  public function getContent(): string;
  public function getFilePath(): string;
  public function getRealPath(): string;
}