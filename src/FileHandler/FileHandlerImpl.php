<?php declare(strict_types=1);

namespace davidschmucker\jsonschemamerger\FileHandler;

class FileHandlerImpl implements FileHandler
{
  private const MIN_CHUNK_SIZE_AND_COUNT = 1;
  private int $fileReadChunkSize = 1024;
  private int $fileReadMaxChunkCount = 100;

  private string $content = '';

  private string $filePath = '';
  private string $realPath = '';

  public function __construct(string $filePath)
  {
    $this->filePath = $filePath;
    $this->realPath = $this->checkFile($filePath);
    $this->readFile($this->realPath);
  }

  public function setFileReadOptionChunk(int $chunkSize, int $chunkCount): bool
  {
    if(($chunkSize < self::MIN_CHUNK_SIZE_AND_COUNT) || ($chunkCount < self::MIN_CHUNK_SIZE_AND_COUNT))
      return false;

    $this->fileReadChunkSize = $chunkSize;
    $this->fileReadMaxChunkCount = $chunkCount;

    return true;
  }

  public function getFileReadOptionChunk(): array
  {
    return [$this->fileReadChunkSize, $this->fileReadMaxChunkCount];
  }

  private function checkFile(string $filePath): string
  {
    if(($realPath = realpath($filePath)) && is_readable($filePath))
      return $realPath;
    else
      throw new \Exception("Can't read file: {$filePath}!");
  }

  private function readFile(string $realPath): bool
  {
    $fileHandle = fopen($realPath, 'r');

    $loopCount = 0;
    while(!feof($fileHandle))
    {
      if($loopCount > $this->fileReadMaxChunkCount)
        throw new \Exception("Max. chunk count for file read ({$realPath}) exeeded!");

      $this->content .= fread($fileHandle, $this->fileReadChunkSize);
      $loopCount++;
    }

    return true;
  }

  public function getContent(): string
  {
    return $this->content;
  }

  public function getFilePath(): string
  {
    return $this->filePath;
  }

  public function getRealPath(): string
  {
    return $this->realPath;
  }
}