# JsonSchemaMerger
Merge References within a JsonSchema into a complete Schema.

----
## Features
available
* merge internal Subschemas References
* merge internal bundled References

in process
* merge extern bundled References

in planing
* merge default File References
* merge relative References
* merge absolute References

----
## Usage
### Json String
```php
$jsonString = '{"$schema":"http://json-schema.org/draft/2020-12/schema","$id":...'; 

$schema = new JsonSchemaImpl($jsonString);
$jsonSchemaMerger = new JsonSchemaMergerImpl($schema, new JsonSchemaFactory());
$mergedSchema = $jsonSchemaMerger->getMergedSchema();

var_dump($mergedSchema->getSchemaString());
var_dump($mergedSchema->getSchemaArray());
var_dump($mergedSchema->getSchemaObject());
```
### Json File
```php
$fh = new FileHandlerImpl('/my/json/file.json');

$schema = new JsonSchemaImpl($fh->getContent());
$jsonSchemaMerger = new JsonSchemaMergerImpl($schema, new JsonSchemaFactory());
$mergedSchema = $jsonSchemaMerger->getMergedSchema();

var_dump($mergedSchema->getSchemaString());
var_dump($mergedSchema->getSchemaArray());
var_dump($mergedSchema->getSchemaObject());
```

----
## Explanation

### Internal Subschema Referenc

Example Json-Schema before Reference Merging:
```json
{
  "$schema": "http://json-schema.org/draft/2020-12/schema",
  "$id": "ns://davidschmucker/jsonschemamerger/Testfiles/testStartFile",
  "title": "Start File Test Schema",
  "description": "Json Schema for Testing JsonSchema Merger",

  "type": "object",
  "properties": {
    "internalSubSchemaRef": {"$ref": "#/$defs/internalSubSchemaRefExample"}
  },

  "$defs": {
    "internalSubSchemaRefExample": {
      "type": "object",
      "properties": {
        "subreference": {"$ref": "#/$defs/subreferenceExample"}
      }
    },
    "subreferenceExample": {
      "type": "string"
    }
  }
}
```

Example Json-Schema after Reference Merging:
```json
{
  "$schema": "http://json-schema.org/draft/2020-12/schema",
  "$id": "ns://davidschmucker/jsonschemamerger/Testfiles/testStartFile",
  "title": "Start File Test Schema",
  "description": "Json Schema for Testing JsonSchema Merger",

  "type": "object",
  "properties": {
    "internalSubSchemaRef": {
      "type": "object",
      "properties": {
        "subreference": {
          "type": "string"
          }
      }
    }
  },

  "$defs": {
    "internalSubSchemaRefExample": {
      "type": "object",
      "properties": {
        "subreference": {"$ref": "#/$defs/subreferenceExample"}
      }
    },
    "subreferenceExample": {
      "type": "string"
    }
  }
}
```

### Internal Bundled References

Example Json-Schema before Reference Merging:
```json
{
  "$schema": "http://json-schema.org/draft/2020-12/schema",
  "$id": "ns://davidschmucker/jsonschemamerger/Testfiles/testStartFile",
  "title": "Start File Test Schema",
  "description": "Json Schema for Testing JsonSchema Merger",

  "type": "object",
  "properties": {
    "bundledRef": {"$ref": "/jsonschemamerger/Testfiles/bundleRef"}
  },

  "$defs": {
    "bundleRefExample": {
      "$schema": "http://json-schema.org/draft/2020-12/schema",
      "$id": "/jsonschemamerger/Testfiles/bundleRef",

      "type": "object",
      "properties": {
        "bundleReference": {"$ref": "/jsonschemamerger/Testfiles/bundleExample"}
      }
    },
    "bundleExample": {
      "$schema": "http://json-schema.org/draft/2020-12/schema",
      "$id": "/jsonschemamerger/Testfiles/bundleExample",

      "type": "string"
    }
  }
}
```

Example Json-Schema after Reference Merging:
```json
{
  "$schema": "http://json-schema.org/draft/2020-12/schema",
  "$id": "ns://davidschmucker/jsonschemamerger/Testfiles/testStartFile",
  "title": "Start File Test Schema",
  "description": "Json Schema for Testing JsonSchema Merger",

  "type": "object",
  "properties": {
    "bundledRef": {
      "type": "object",
      "properties": {
        "bundleReference": {
          "type": "string"
        }
      }
    }
  },

  "$defs": {
    "bundleRefExample": {
      "$schema": "http://json-schema.org/draft/2020-12/schema",
      "$id": "/jsonschemamerger/Testfiles/bundleRef",

      "type": "object",
      "properties": {
        "bundleReference": {"$ref": "/jsonschemamerger/Testfiles/bundleExample"}
      }
    },
    "bundleExample": {
      "$schema": "http://json-schema.org/draft/2020-12/schema",
      "$id": "/jsonschemamerger/Testfiles/bundleExample",

      "type": "string"
    }
  }
}
```