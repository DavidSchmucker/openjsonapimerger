<?php declare(strict_types=1);

namespace davidschmucker\jsonschemamerger\Tests\Units;

use davidschmucker\jsonschemamerger\FileHandler\FileHandlerImpl;
use davidschmucker\jsonschemamerger\JsonSchema\JsonSchema;
use davidschmucker\jsonschemamerger\JsonSchema\JsonSchemaFactory;
use davidschmucker\jsonschemamerger\JsonSchema\JsonSchemaImpl;
use davidschmucker\jsonschemamerger\JsonSchemaMerger\JsonSchemaMergerImpl;

use PHPUnit\Framework\TestCase;


final class JsonSchemaMergerTest extends TestCase
{
  private static string $schemaDirectory = __DIR__ . '/../Testfiles/';
  private static array $schemaArray;

  /**
   * @beforeClass
   */
  public static function setUpNewJsonSchema(): void
  {
    $fh = new FileHandlerImpl(JsonSchemaMergerTest::$schemaDirectory . 'testStartFile');
    $schema = new JsonSchemaImpl($fh->getContent());
    $newJsonSchema = new JsonSchemaMergerImpl($schema, new JsonSchemaFactory());
    $jsonSchema = $newJsonSchema->getMergedSchema();
    JsonSchemaMergerTest::$schemaArray = $jsonSchema->getSchemaArray();
  }

  public function testInternalSubSchema(): void
  {
    $this->assertIsArray(JsonSchemaMergerTest::$schemaArray['properties']['internalSubSchemaRef']);
    $this->assertEquals(
      'object',
      JsonSchemaMergerTest::$schemaArray['properties']['internalSubSchemaRef']['type'] 
    );
    $this->assertIsArray(JsonSchemaMergerTest::$schemaArray['properties']['internalSubSchemaRef']['properties']);
    $this->assertIsArray(JsonSchemaMergerTest::$schemaArray['properties']['internalSubSchemaRef']['properties']['subreference']);
    $this->assertEquals(
      'string',
      JsonSchemaMergerTest::$schemaArray['properties']['internalSubSchemaRef']['properties']['subreference']['type']
    );
  }

  public function testInternalBundle(): void
  {
    $this->assertIsArray(JsonSchemaMergerTest::$schemaArray['properties']['bundledRef']);
    $this->assertEquals(
      'object',
      JsonSchemaMergerTest::$schemaArray['properties']['bundledRef']['type'] 
    );
    $this->assertIsArray(JsonSchemaMergerTest::$schemaArray['properties']['bundledRef']['properties']);
    $this->assertIsArray(JsonSchemaMergerTest::$schemaArray['properties']['bundledRef']['properties']['bundleReference']);
    $this->assertEquals(
      'string',
      JsonSchemaMergerTest::$schemaArray['properties']['bundledRef']['properties']['bundleReference']['type']
    );
  }
}