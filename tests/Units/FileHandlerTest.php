<?php declare(strict_types=1);

namespace davidschmucker\jsonschemamerger\Tests\Units;

use davidschmucker\jsonschemamerger\FileHandler\FileHandlerImpl;
use PHPUnit\Framework\TestCase;

final class FileHandlerTest extends TestCase
{
  private string $testFilePath = __DIR__ . '/../Testfiles/openapi';

  private string $endlessFilePath = '/dev/urandom';

  public function testChunkOption(): void
  {
    $fh = new FileHandlerImpl($this->testFilePath);

    $this->assertFalse($fh->setFileReadOptionChunk(-1, 1));
    $this->assertFalse($fh->setFileReadOptionChunk(1, -1));
    $this->assertTrue($fh->setFileReadOptionChunk(1, 2));
    $this->assertEquals([1, 2], $fh->getFileReadOptionChunk());
  }

  public function testFilePath(): void
  {
    $filePerm = octdec(substr(sprintf('%o', fileperms($this->testFilePath)), -4));
    try
    {
      chmod($this->testFilePath, 0000);
      new FileHandlerImpl($this->testFilePath);
    }
    catch(\Exception $e)
    {
      $this->assertEquals(
        "Can't read file: {$this->testFilePath}!",
        $e->getMessage()
      );
    }
    finally
    {
      chmod($this->testFilePath, $filePerm);
    }

    try
    {
      new FileHandlerImpl('abc');
    }
    catch(\Exception $e)
    {
      $this->assertEquals(
        'Can\'t read file: abc!',
        $e->getMessage()
      );
    }
  }

  public function testReadFile(): void
  {
    try
    {
      new FileHandlerImpl($this->endlessFilePath);
    }
    catch(\Exception $e)
    {
      $this->assertEquals(
        "Max. chunk count for file read ({$this->endlessFilePath}) exeeded!",
        $e->getMessage()
      );
    }

    $fh = new FileHandlerImpl($this->testFilePath);

    $this->assertEquals(
      file_get_contents(realpath($this->testFilePath)),
      $fh->getContent()
    );
  }
}