<?php declare(strict_types=1);

namespace davidschmucker\jsonschemamerger\Tests\Units;

use davidschmucker\jsonschemamerger\JsonSchema\JsonSchemaFactory;
use PHPUnit\Framework\TestCase;

final class JsonSchemaFactoryTest extends TestCase
{
  private string $testFilePath = __DIR__ . '/../Testfiles/openapi';

  public function testJsonSchemaFileCreation(): void
  {
    $factory = new JsonSchemaFactory();
    $schema = $factory->createFromFile($this->testFilePath);
    $this->assertEquals(
      '304c3ab4f9cf21afc650723c5e7e8ac0',
      hash('md5', $schema->getSchemaString())
    );
  }

  public function testJsonSchemaArrayCreation(): void
  {
    $factory = new JsonSchemaFactory();
    $array = ['abc' => ['def' => 123]];
    $schema = $factory->createFromArray($array);

    $this->assertEquals(
      '{"abc":{"def":123}}',
      $schema->getSchemaString()
    );
  }
}