<?php declare(strict_types=1);

namespace davidschmucker\jsonschemamerger\Tests\Units;

use PHPUnit\Framework\TestCase;

use davidschmucker\jsonschemamerger\JsonSchema\JsonSchemaCollection;
use davidschmucker\jsonschemamerger\JsonSchema\JsonSchemaImpl;

final class JsonSchemaTest extends TestCase
{
  private string $nonJsonString = 'abc';
  private string $jsonString = '{"abc":123}';
  private string $jsonString2 = '{"test":456,"sub":{"array":[1,2,3]}}';

  public function testSchemaInputString(): void
  {
    try
    {
      new JsonSchemaImpl($this->nonJsonString);
    }
    catch(\Exception $e)
    {
      $this->assertTrue(
        str_starts_with($e->getMessage(), 'Schema String doesn\'t contain a valid JSON!')
      );
    }

    $oas = new JsonSchemaImpl($this->jsonString);
    $this->assertEquals($this->jsonString, $oas->getSchemaString());
    $this->assertEquals(json_decode($this->jsonString, true), $oas->getSchemaArray());
    $this->assertEquals(json_decode($this->jsonString, false), $oas->getSchemaObject());
  }

  public function testSchemaInputNull(): void
  {
    $schema = new JsonSchemaImpl();

    $this->assertNull($schema->getSchemaString());
    $this->assertNull($schema->getSchemaArray());
    $this->assertNull($schema->getSchemaObject());
  }

  public function testSchemaSetter(): void
  {
    $schema = new JsonSchemaImpl($this->jsonString);
    $schemaString = $schema->getSchemaString();
    $schemaArray = $schema->getSchemaArray();
    $schemaObject = $schema->getSchemaObject();


    $schema->setSchemaByString($this->jsonString2);
    $this->assertNotEquals($schemaString, $schema->getSchemaString());
    $this->assertNotEquals($schemaArray, $schema->getSchemaArray());
    $this->assertNotEquals($schemaObject, $schema->getSchemaObject());

    $schema->setSchemaByString($this->jsonString);
    $this->assertEquals($schemaString, $schema->getSchemaString());
    $this->assertEquals($schemaArray, $schema->getSchemaArray());
    $this->assertEquals($schemaObject, $schema->getSchemaObject());


    $schema->setSchemaByArray(json_decode($this->jsonString2, true));
    $this->assertNotEquals($schemaString, $schema->getSchemaString());
    $this->assertNotEquals($schemaArray, $schema->getSchemaArray());
    $this->assertNotEquals($schemaObject, $schema->getSchemaObject());

    $schema->setSchemaByArray(json_decode($this->jsonString, true));
    $this->assertEquals($schemaString, $schema->getSchemaString());
    $this->assertEquals($schemaArray, $schema->getSchemaArray());
    $this->assertEquals($schemaObject, $schema->getSchemaObject());


    $schema->setSchemaByObject(json_decode($this->jsonString2, false));
    $this->assertNotEquals($schemaString, $schema->getSchemaString());
    $this->assertNotEquals($schemaArray, $schema->getSchemaArray());
    $this->assertNotEquals($schemaObject, $schema->getSchemaObject());

    $schema->setSchemaByObject(json_decode($this->jsonString, false));
    $this->assertEquals($schemaString, $schema->getSchemaString());
    $this->assertEquals($schemaArray, $schema->getSchemaArray());
    $this->assertEquals($schemaObject, $schema->getSchemaObject());
  }

  public function testSchemaCollection(): void
  {
    $schema1 = new JsonSchemaImpl($this->jsonString);
    $schema2 = new JsonSchemaImpl($this->jsonString);
    $schemas = new JsonSchemaCollection($schema1, $schema2);
    $this->assertCount(2, $schemas);
    $this->assertTrue($schemas->offsetExists(0));
    $this->assertTrue($schemas->offsetExists(1));
    $this->assertFalse($schemas->offsetExists(2));
    $this->assertEquals($schema1, $schemas->offsetGet(0));
    $schema3 = new JsonSchemaImpl($this->jsonString);
    $schemas->offsetSet(2, $schema3);
    $this->assertTrue($schemas->offsetExists(2));
    try {$schemas->offsetSet(3, $schema3);} catch(\Exception $e) {
      $this->assertEquals(
        'Value always exists in Collection!',
        $e->getMessage()
      );
    };
    try {$schemas->offsetSet(3, 123);} catch(\InvalidArgumentException $e) {
      $this->assertEquals(
        'Value is not a JsonSchema!',
        $e->getMessage()
      );
    };
    $schemas->offsetUnset(2);
    $this->assertFalse($schemas->offsetExists(2));

    $iterator = $schemas->getIterator();
    $this->assertEquals($schema1, $iterator->current());
    $iterator->next();
    $this->assertEquals($schema2, $iterator->current());
    $iterator->next();
    $this->assertEquals(null, $iterator->current());

    try
    {
      $schema = new JsonSchemaImpl($this->jsonString);
      $schemas = new JsonSchemaCollection($schema, $schema);
    }
    catch(\Exception $e)
    {
      $this->assertEquals(
        'Double value exists in Parameter!',
        $e->getMessage()
      );
    }

    try
    {
      $schema = new JsonSchemaImpl($this->jsonString);
      $schemas = new JsonSchemaCollection($schema);
      $schemas->offsetSet(count($schemas), $schema);
    }
    catch(\Exception $e)
    {
      $this->assertEquals(
        'Value always exists in Collection!',
        $e->getMessage()
      );
    }

  }
}